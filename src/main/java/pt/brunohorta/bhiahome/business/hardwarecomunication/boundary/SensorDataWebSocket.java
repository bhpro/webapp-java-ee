package pt.brunohorta.bhiahome.business.hardwarecomunication.boundary;


import pt.brunohorta.bhiahome.business.hardwarecomunication.sensorparsers.MCUBasementParser;
import pt.brunohorta.bhiahome.business.hardwarecomunication.sensorparsers.MCUParseException;

import javax.ejb.Singleton;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@Singleton
@ServerEndpoint("/hardware")
public class SensorDataWebSocket {

    @OnClose
    public void close(Session session) {
        System.out.println(session);
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        MCUBasementParser.SensorData parse = null;
        try {
            parse = MCUBasementParser.parse(message);
            System.out.println(parse.toString());
        } catch (MCUParseException e) {
            System.out.println(e.getMessage());
        }
    }


    @OnOpen
    public void open(Session session) {
        System.out.println(session);
    }


}
