package pt.brunohorta.bhiahome.business.hardwarecomunication.sensorparsers;

/**
 * Created by brunohorta on 30/05/16.
 */

public class MCUParseException extends Exception {
    public MCUParseException() {
        super("MCU DATA PARSE ERROR");
    }
}
