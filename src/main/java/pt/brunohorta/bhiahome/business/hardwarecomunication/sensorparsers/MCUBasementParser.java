package pt.brunohorta.bhiahome.business.hardwarecomunication.sensorparsers;


import java.util.Objects;

/**
 * Created by brunohorta on 30/05/16.
 */
public class MCUBasementParser {
    public static final int DEFAULT_LIGHT_VOLTAGE = 220;

    public static SensorData parse(String s) throws MCUParseException {

        String[] split = s.split(",");
        if (split.length == 6) {
            return new SensorData(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]), Double.parseDouble(split[4]), !Objects.equals(split[5], "0"));
        }
        throw new MCUParseException();
    }

    public static class SensorData {
        private int voltageL1;
        private int voltageL2;
        private int voltageL3;
        private int voltageL4;
        private boolean l1ON;
        private boolean l2ON;
        private boolean l3ON;
        private boolean l4ON;
        private double lux;
        private boolean movementDetected;

        public SensorData(int voltageL1, int voltageL2, int voltageL3, int voltageL4, double lux, boolean movementDetected) {
            this.voltageL1 = voltageL1 > 0 ? voltageL1 + 3 : voltageL1;
            this.voltageL2 = voltageL2;
            this.voltageL3 = voltageL3;
            this.voltageL4 = voltageL4;
            this.movementDetected = movementDetected;
            l1ON = this.voltageL1 >= DEFAULT_LIGHT_VOLTAGE;
            l2ON = this.voltageL2 >= DEFAULT_LIGHT_VOLTAGE;
            l3ON = this.voltageL3 >= DEFAULT_LIGHT_VOLTAGE;
            l4ON = this.voltageL4 >= DEFAULT_LIGHT_VOLTAGE;
            this.lux = lux;
        }

        public int getVoltageL1() {
            return voltageL1;
        }

        public void setVoltageL1(int voltageL1) {
            this.voltageL1 = voltageL1;
        }

        public int getVoltageL2() {
            return voltageL2;
        }

        public void setVoltageL2(int voltageL2) {
            this.voltageL2 = voltageL2;
        }

        public int getVoltageL3() {
            return voltageL3;
        }

        public void setVoltageL3(int voltageL3) {
            this.voltageL3 = voltageL3;
        }

        public int getVoltageL4() {
            return voltageL4;
        }

        public void setVoltageL4(int voltageL4) {
            this.voltageL4 = voltageL4;
        }

        public double getLux() {
            return lux;
        }

        public void setLux(double lux) {
            this.lux = lux;
        }

        public boolean isL1ON() {
            return l1ON;
        }

        public void setL1ON(boolean l1ON) {
            this.l1ON = l1ON;
        }

        public boolean isL2ON() {
            return l2ON;
        }

        public void setL2ON(boolean l2ON) {
            this.l2ON = l2ON;
        }

        public boolean isL3ON() {
            return l3ON;
        }

        public void setL3ON(boolean l3ON) {
            this.l3ON = l3ON;
        }

        public boolean isL4ON() {
            return l4ON;
        }

        public void setL4ON(boolean l4ON) {
            this.l4ON = l4ON;
        }

        public boolean isMovementDetected() {
            return movementDetected;
        }

        public void setMovementDetected(boolean movementDetected) {
            this.movementDetected = movementDetected;
        }

        @Override
        public String toString() {
            return "SensorData{" +
                    "voltageL1=" + voltageL1 +
                    ", voltageL2=" + voltageL2 +
                    ", voltageL3=" + voltageL3 +
                    ", voltageL4=" + voltageL4 +
                    ", l1ON=" + l1ON +
                    ", l2ON=" + l2ON +
                    ", l3ON=" + l3ON +
                    ", l4ON=" + l4ON +
                    ", lux=" + lux +
                    ", movementDetected=" + movementDetected +
                    '}';
        }
    }


}
