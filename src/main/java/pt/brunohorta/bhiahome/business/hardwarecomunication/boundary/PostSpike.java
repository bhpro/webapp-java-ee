package pt.brunohorta.bhiahome.business.hardwarecomunication.boundary;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by brunohorta on 31/05/16.
 */
@Path("arduino")
@Stateless
public class PostSpike {

    @POST
    @Path("/")
    @Consumes("application/json")
    public Response postTest(String ola) {
        return Response.ok("POST OK").build();
    }
    @GET
    @Path("/")
    @Produces("application/json")
    public Response getTest() {
        return Response.ok("GET OK").build();
    }
}
